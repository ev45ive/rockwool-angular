import { NgModule } from '@angular/core';
import { Routes, RouterModule, ROUTES } from '@angular/router';
import { CounterComponent } from './playlists/counter/counter.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'music',
    pathMatch: 'full'
  },
  {
    path: 'counter',
    component: CounterComponent
  },
  {
    path: 'music',
    loadChildren: () => import('./music-search/music-search.module')
      .then(m => m.MusicSearchModule)
  },
  {
    path: 'playground',
    loadChildren: () => import('./playground/playground.module')
      .then(m => m.PlaygroundModule)
  },
  {
    path: '**',
    // component: PageNotFoundComponent //  shared?
    redirectTo: 'music',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true // HTML5 history pushState API
    // paramsInheritanceStrategy:'emptyOnly'
    paramsInheritanceStrategy: 'always',
    enableTracing: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
