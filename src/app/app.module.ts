import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { CoreModule } from './core/core.module';
import { playlistsMock } from './core/data/playlists.mock';
import { ALBUMS_MOCK, PLAYLISTS_MOCK } from './core/tokens';
import { albumsMock } from './core/data/albums.mock';
import { SharedModule } from './shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule, EffectsRootModule } from '@ngrx/effects';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    // Libraries
    BrowserModule,
    SharedModule,
    // Feature Modules
    PlaylistsModule,
    // Root Router
    AppRoutingModule,
    StoreModule.forRoot(reducers/* , { metaReducers } */),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    {
      provide: PLAYLISTS_MOCK,
      useValue: playlistsMock
    },
    {
      provide: ALBUMS_MOCK,
      useValue: albumsMock
    },
  ],
  // bootstrap: [AppComponent/* HeaderComponent, WidgetComponent */]
})
export class AppModule implements DoBootstrap {
  ngDoBootstrap(appRef: ApplicationRef): void {
    // fetchServerConfig().then(config => {...
    appRef.bootstrap(AppComponent, 'app-root')
    // appRef.bootstrap(AppComponent, 'app-root2')
    // appRef.bootstrap(AppComponent, 'app-root3')
    // })
  }
}
