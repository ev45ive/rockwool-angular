import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCounter from './counter/counter.reducer';
// import * as fromMusicSearch from '../music-search/reducers/music-search.reducer';


export interface AppState {
  [fromCounter.counterFeatureKey]: fromCounter.State;
  // [fromMusicSearch.musicSearchFeatureKey]: fromMusicSearch.State;
}

export const reducers: ActionReducerMap<AppState> = {
  [fromCounter.counterFeatureKey]: fromCounter.reducer,
  // [fromMusicSearch.musicSearchFeatureKey]: fromMusicSearch.reducer,
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
