import { Action, createReducer, on } from '@ngrx/store';
import { decrementCounter, incrementCounter, loadCounters } from 'src/app/actions/counter/counter.actions';


export const counterFeatureKey = 'counter';

export interface State {
  counter: number
}

export const initialState: State = {
  counter: 0
};


export const reducer = createReducer(
  initialState,
  on(loadCounters, (state, action) => ({ ...state, counter: 0 })),
  on(incrementCounter, (state, action) => {
    debugger
    return ({ ...state, counter: state.counter + action.amount })
  }),
  on(decrementCounter, (state, action) => ({ ...state, counter: state.counter - action.amount })),
);

