import { Action, createReducer, on } from '@ngrx/store';
import { Album } from 'src/app/core/models/album';
import * as MusicSearchActions from '../../actions/music-search/music-search.actions';

export const musicSearchFeatureKey = 'musicSearch';

export interface State {
  query: string,
  results: Album[],
  loading: boolean,
  message: string
}

export const initialState: State = {
  query: '', loading: false, results: [], message: ''
};


export const reducer = createReducer(
  initialState,

  on(MusicSearchActions.musicSearchStart, (state, action) => ({
    ...state, query: action.query, loading: true
  })),
  on(MusicSearchActions.musicSearchSuccess, (state, action) => ({
    ...state, results: action.data, loading: false
  })),
  on(MusicSearchActions.musicSearchFailure, (state, action) => ({
    ...state, message: action.error?.message, loading: false
  })),

);

