import { Component, ContentChildren, OnInit, QueryList, ViewChildren } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  // @ContentChildren(TabComponent)
  // children?: QueryList<TabComponent>;

  children: TabComponent[] = [];

  constructor() { }

  toggle(active: TabComponent) {
    this.children.forEach(tab => {
      tab.open = tab === active
    })
  }
  register(tab: TabComponent) {
    this.children.push(tab)
  }

  unregister(tab: TabComponent) {
    this.children.splice(this.children.indexOf(tab))
  }

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    console.log(this.children)
  }

}
