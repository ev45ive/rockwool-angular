import { Component, Inject, Input, OnInit, Optional } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() title: string = '';

  open = false

  toggle() {
    if (parent) {
      this.parent.toggle(this)
    } else {
      this.open = !this.open
    }
  }

  constructor(
    // @Inject(TabsComponent)
    @Optional() private parent: TabsComponent) {

    this.parent?.register(this)
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.parent.unregister(this)
  }

}
