import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { iif, of } from 'rxjs';
import { map, pluck, share, switchMap, tap } from 'rxjs/operators';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';
import { PlaylistsComponent } from '../playlists/playlists.component';

@Component({
  selector: 'app-selected-playlist-container',
  templateUrl: './selected-playlist-container.component.html',
  styleUrls: ['./selected-playlist-container.component.scss']
})
export class SelectedPlaylistContainerComponent implements OnInit {

  selected = this.route.data.pipe(pluck('playlist'))

  constructor(
    public router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
