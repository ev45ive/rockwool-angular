import { Component, CUSTOM_ELEMENTS_SCHEMA, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { playlistsMock } from 'src/app/core/data/playlists.mock';
import { CardComponent } from 'src/app/shared/card/card.component';

import { SelectedPlaylistContainerComponent } from './selected-playlist-container.component';

@Component({ selector: 'app-playlist-details', template: '' })
export class MockPlaylistDetailsComponent { @Input() playlist: any }

fdescribe('SelectedPlaylistContainerComponent', () => {
  let component: SelectedPlaylistContainerComponent;
  let fixture: ComponentFixture<SelectedPlaylistContainerComponent>;
  let route: ActivatedRoute

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SelectedPlaylistContainerComponent, MockPlaylistDetailsComponent],
      schemas:[CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: Router, useFactory: () => jasmine.createSpyObj('Router', [], ['navigate']) },
        {
          provide: ActivatedRoute,
          useFactory: () => ({ data: {} }) //jasmine.createSpyObj('ActivatedRoute', [], ['data'])
        }
      ]
    })
      .compileComponents();
  });


  it('should subscribe to route data', () => {
    const fakeData = new Subject()
    route = TestBed.inject(ActivatedRoute);
    // const spy = spyOnProperty(route, 'data').and.returnValue(fakeData)
    (route as any).data = fakeData

    fixture = TestBed.createComponent(SelectedPlaylistContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
    fakeData.next({ playlist: playlistsMock[0] })
    fixture.detectChanges();
    
    const mock = fixture.debugElement.query(By.directive(MockPlaylistDetailsComponent))

    expect(mock.componentInstance.playlist).toEqual(playlistsMock[0]);
  });
});
