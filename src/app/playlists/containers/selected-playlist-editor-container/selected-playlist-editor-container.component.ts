import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { pluck } from 'rxjs/operators';
import { Playlist } from 'src/app/core/models/playlist';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';

@Component({
  selector: 'app-selected-playlist-editor-container',
  templateUrl: './selected-playlist-editor-container.component.html',
  styleUrls: ['./selected-playlist-editor-container.component.scss']
})
export class SelectedPlaylistEditorContainerComponent implements OnInit {

  selected = this.route.data.pipe(pluck('playlist'))

  allChangesAreSaved = false;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private playlistService: PlaylistsService) { }

  ngOnInit(): void {
    this.selected.subscribe(console.log)
  }

  cancel() {
    this.allChangesAreSaved = true;
    this.router.navigate(['/playlists'])
  }

  savePlaylistChanges(draft: Playlist) {
    this.playlistService.savePlaylist(draft).subscribe(() => {
      this.allChangesAreSaved = true;
      this.router.navigate(['/playlists', draft.id])
    })
  }

}
