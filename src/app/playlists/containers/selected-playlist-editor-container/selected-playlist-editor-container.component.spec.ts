import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedPlaylistEditorContainerComponent } from './selected-playlist-editor-container.component';

describe('SelectedPlaylistEditorContainerComponent', () => {
  let component: SelectedPlaylistEditorContainerComponent;
  let fixture: ComponentFixture<SelectedPlaylistEditorContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectedPlaylistEditorContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedPlaylistEditorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
