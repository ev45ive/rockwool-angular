import { NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationCancel } from '@angular/router';
import { EMPTY, from, iif, merge, of, Subject } from 'rxjs';
import { map, mergeAll, share, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { Playlist } from 'src/app/core/models/playlist';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';

NgIf

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  message = ''

  constructor(private router: Router) {

    router.events.subscribe(event => {

      if (event instanceof NavigationCancel) {
        this.message = "Navigation cancelled " + event.reason;
      }

    })

  }

  ngOnInit(): void {
  }
}
