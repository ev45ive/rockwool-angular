import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Playlist } from 'src/app/core/models/playlist';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';

@Component({
  selector: 'app-user-playlists',
  templateUrl: './user-playlists.component.html',
  styleUrls: ['./user-playlists.component.scss']
})
export class UserPlaylistsComponent implements OnInit {

  selectedId = this.route.paramMap.pipe(
    map(pm => pm.get('playlist_id')))

  playlistsChange = this.playlistService.playlistsChange
  playlists: Playlist[] = []

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistService: PlaylistsService) { }

  ngOnInit(): void {
    this.playlistService.loadPlaylists()
  }

  selectPlaylist(id: Playlist['id']) {
    this.router.navigate(['/playlists', id], {
      relativeTo: this.route,
    })
  }

  cancel() {
    this.router.navigate(['/playlists'], {})
  }
}
