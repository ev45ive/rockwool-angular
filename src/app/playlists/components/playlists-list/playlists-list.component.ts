import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Playlist } from 'src/app/core/models/playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
  // inputs:['playlists:items']
  // encapsulation: ViewEncapsulation.Emulated
})
export class PlaylistsListComponent implements OnInit {

  // ng-input
  @Input('items') playlists: Playlist[] | null = []

  @Input() selected: Playlist['id'] | null = null

  // ng-output
  @Output() selectedChange = new EventEmitter<Playlist['id']>()

  select(id: Playlist['id']) {
    this.selectedChange.emit(id/* == $event */)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
