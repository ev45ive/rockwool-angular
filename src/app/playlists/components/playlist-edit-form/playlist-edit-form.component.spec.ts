import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { PlaylistEditFormComponent } from './playlist-edit-form.component';

import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed'
import { HarnessLoader } from '@angular/cdk/testing';
import { By } from '@angular/platform-browser';
import { playlistsMock } from 'src/app/core/data/playlists.mock';
import { FormsModule, NgModel } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler';

describe('PlaylistEditFormComponent', () => {
  let component: PlaylistEditFormComponent;
  let fixture: ComponentFixture<PlaylistEditFormComponent>;
  let loader: HarnessLoader
  let rootLoader: HarnessLoader

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistEditFormComponent],
      schemas:[CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule]
    }).compileComponents()
    fixture = TestBed.createComponent(PlaylistEditFormComponent);
    // loader = TestbedHarnessEnvironment.loader(fixture)
    // rootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture)
    component = fixture.componentInstance;
    component.playlist = playlistsMock[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show name input', fakeAsync(async () => {
    component.ngOnChanges({})
    tick()
    fixture.detectChanges()
    const input = fixture.debugElement.query(By.css('[name="name"]'))

    expect(input.nativeElement.value).toEqual('pLaylist 123');
  }));

  it('submit form changes', async () => {
    const inputs = fixture.debugElement.queryAll(By.directive(NgModel))

    inputs[0].nativeElement.value = 'zmiana'
    inputs[0].triggerEventHandler('input', { target: inputs[0].nativeElement })

    expect(inputs[0].injector.get(NgModel).value).toEqual('zmiana')

    const submitBtn = fixture.debugElement.query(By.css('[type="submit"]'));

    const spy = jasmine.createSpy('Submit spy')
    component.save.subscribe(spy)
    // submitBtn.triggerEventHandler('click', {})
    submitBtn.nativeElement.dispatchEvent(new MouseEvent('click'))

    expect(spy).toHaveBeenCalledWith(jasmine.objectContaining({
      id: jasmine.stringMatching(playlistsMock[0].id),
      name: 'zmiana'
    }))

  });
});
