import { AfterViewChecked, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { CheckboxControlValueAccessor, NgForm, NgModel } from '@angular/forms';
import { Playlist } from 'src/app/core/models/playlist';

CheckboxControlValueAccessor

@Component({
  selector: 'app-playlist-edit-form',
  templateUrl: './playlist-edit-form.component.html',
  styleUrls: ['./playlist-edit-form.component.scss'],
  // changeDetection:ChangeDetectionStrategy.OnPush
})
export class PlaylistEditFormComponent implements OnInit, OnChanges, AfterViewChecked {

  @Input() playlist!: Playlist
  draft!: Playlist

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  cancelClick() {
    this.cancel.emit()
  }

  @ViewChild('formRef', { read: NgForm })
  formRef?: NgForm

  submitForm(formRef: NgForm) {
    const draft: Playlist = {
      ...this.playlist,
      ...formRef.value
    }
    this.save.emit(draft)
  }

  constructor(cdr: ChangeDetectorRef) {
    // console.log('constructor')
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // console.log('ngOnChanges', changes)
    // this.formRef
    // debugger

    setTimeout(()=>{
      this.formRef?.setValue({
        name: this.playlist.name,
        public: this.playlist.public,
        description: this.playlist.description,
      })
    })
  }

  ngOnInit(): void {
    // this.draft = { ...this.playlist }
    // console.log('ngOnInit')
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    // console.log('ngDoCheck')
  }

  @ViewChild('nameRef', { read: ElementRef })
  nameFieldRef?: ElementRef<HTMLInputElement>

  @ViewChild('nameRef', { read: NgModel })
  nameModelRef?: NgModel

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // console.log('ngAftengAfterViewInitrViewChecked')
    this.nameFieldRef?.nativeElement.focus()
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    // console.log('ngAfterViewChecked')
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // console.log('ngOnDestroy')
  }

}
