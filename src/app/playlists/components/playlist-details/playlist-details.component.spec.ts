import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { playlistsMock } from 'src/app/core/data/playlists.mock';
import { CardComponent } from 'src/app/shared/card/card.component';
import { YesnoPipe } from 'src/app/shared/yesno.pipe';

import { PlaylistDetailsComponent } from './playlist-details.component';

// xdescribe('PlaylistDetailsComponent', () => {
describe('PlaylistDetailsComponent', () => {
  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;
  let element: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent, YesnoPipe, CardComponent],
      imports: []
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent)
    component = fixture.componentInstance
    element = fixture.debugElement

    component.playlist = playlistsMock[0]
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render playlist name', () => {
    // expect(element.nativeElement.innerHTML).toContain('Playlist 123');

    const title = element.query(By.css('[data-test-id="title"]'))
    expect(title.nativeElement.textContent).toEqual('Playlist 123')
  });

  it('emits (edit) event when Edit button clicked', () => {
    const btn = element.query(By.css('[data-test-id="edit-btn"]'))
    const spy = jasmine.createSpy('Edit Event')
    component.edit.subscribe(spy)

    // btn.nativeElement.dispatchEvent(new MouseEvent('click'))
    btn.triggerEventHandler('click', { target: {} })
    expect(spy).toHaveBeenCalled()
  })


});
