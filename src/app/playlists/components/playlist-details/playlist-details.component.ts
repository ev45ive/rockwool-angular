import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/models/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist!: Playlist

  @Output() edit = new EventEmitter<{id:Playlist['id']}>();
  @Output() cancel = new EventEmitter();



  constructor() {
    // this.playlist.tracks.push({
    //   id: '123',
    //   name:'tes'
    // })
  }

  ngOnInit(): void {
    if (!this.playlist) { throw new Error('PLaylist data is undefind') }
  }

}
