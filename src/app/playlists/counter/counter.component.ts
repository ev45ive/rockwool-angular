import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { decrementCounter, incrementCounter } from 'src/app/actions/counter/counter.actions';
import * as fromStore from '../../reducers';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  counter = this.store.select(state => state.counter.counter);

  constructor(private store: Store<fromStore.AppState>) { }

  increment() {
    this.store.dispatch(incrementCounter({ amount: 1 }))
  }

  decrement() {
    this.store.dispatch(decrementCounter({ amount: 1 }))
  }

  ngOnInit(): void {
  }

}
