import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './containers/playlists/playlists.component';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistEditFormComponent } from './components/playlist-edit-form/playlist-edit-form.component';
import { PlaylistsListItemComponent } from './components/playlists-list-item/playlists-list-item.component';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { SelectedPlaylistContainerComponent } from './containers/selected-playlist-container/selected-playlist-container.component';
import { SelectedPlaylistEditorContainerComponent } from './containers/selected-playlist-editor-container/selected-playlist-editor-container.component';
import { UserPlaylistsComponent } from './containers/user-playlists/user-playlists.component';
import { CounterComponent } from './counter/counter.component';

@NgModule({
  declarations: [
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    PlaylistEditFormComponent,
    PlaylistsListItemComponent,
    SelectedPlaylistContainerComponent,
    SelectedPlaylistEditorContainerComponent,
    UserPlaylistsComponent,
    CounterComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule,
    CoreModule
  ],
  exports: [
    CoreModule
  ],
  providers: [
  ]
})
export class PlaylistsModule { }
