import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistPrivateGuard } from '../core/guards/playlist-private.guard';
import { PlaylistResolver } from '../core/resolvers/playlist.resolver';
import { PlayerComponent } from '../shared/components/player/player.component';
import { RouterMessageComponent } from '../shared/components/router-message/router-message.component';
import { PlaylistsComponent } from './containers/playlists/playlists.component';
import { SelectedPlaylistContainerComponent } from './containers/selected-playlist-container/selected-playlist-container.component';
import { SelectedPlaylistEditorContainerComponent } from './containers/selected-playlist-editor-container/selected-playlist-editor-container.component';
import { UserPlaylistsComponent } from './containers/user-playlists/user-playlists.component';

const routes: Routes = [
  {
    path: 'playlists',
    component: PlaylistsComponent,
    children: [
      {
        path: '', component: UserPlaylistsComponent, outlet: 'list'
      },
      {
        path: '',
        component: RouterMessageComponent,
        data: {
          message: 'Please select playlist'
        }
      },
    ]
  }, {
    path: 'playlists/:playlist_id',
    component: PlaylistsComponent,
    resolve: {
      // playlists: CurrentUserPlaylistsResolver
      playlist: PlaylistResolver
    },
    // runGuardsAndResolvers:'pathParamsChange',
    children: [
      {
        path: '', component: UserPlaylistsComponent, outlet: 'list'
      },
      {
        path: '',
        component: SelectedPlaylistContainerComponent,
        data: {
          roles: ['admin', 'private'] /* User Scopes, etc... */
        },
        canActivate: [PlaylistPrivateGuard],
      },
      {
        path: 'edit',
        component: SelectedPlaylistEditorContainerComponent,
        canDeactivate: [PlaylistPrivateGuard]
      }
    ]
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
