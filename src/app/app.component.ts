import { Component } from '@angular/core';
import { AuthService } from './core/services/auth/auth.service';

@Component({
  selector: 'app-root, app-placki',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular Rockwool';

  open = false

  constructor(private auth: AuthService) { }

  login() {
    this.auth.login()
  }
}
