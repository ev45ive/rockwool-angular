import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromMusicSearch from '../../reducers/music-search/music-search.reducer';

export const selectMusicSearchState = createFeatureSelector<fromMusicSearch.State>(
  fromMusicSearch.musicSearchFeatureKey
);

export const selectSearchResults = createSelector(
  selectMusicSearchState,
  (state) => state.results
)

export const selectLoading = createSelector(
  selectMusicSearchState,
  (state) => state.loading
)

export const selectSearchQuery = createSelector(
  selectMusicSearchState,
  (state) => state.query
)

export const selectErrorMessage = createSelector(
  selectMusicSearchState,
  (state) => state.message
)