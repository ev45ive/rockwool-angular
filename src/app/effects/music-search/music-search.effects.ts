import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, switchMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';

import * as MusicSearchActions from '../../actions/music-search/music-search.actions';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';



@Injectable()
export class MusicSearchEffects {

  constructor(private actions$: Actions,
    private service: MusicSearchService) { }


  loadMusicSearchs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MusicSearchActions.musicSearchStart/* , ... */),
      switchMap((action) => {

        const actions = this.service.fetchSearchAlbumsResult(action.query).pipe(
          map(data => MusicSearchActions.musicSearchSuccess({ data })),
          catchError(error => of(MusicSearchActions.musicSearchFailure({ error })))
        )

        return actions
      })
    );
  });

  /** An EMPTY observable only emits completion. Replace with your own observable API request */
  // EMPTY.pipe(
  //   map(data => MusicSearchActions.musicSearchSuccess({ data })),
  //   catchError(error => of(MusicSearchActions.musicSearchFailure({ error }))))
  // return of(MusicSearchActions.musicSearchSuccess({ data: [] }))

  //   ,
  // this.service.fetchSearchAlbumsResult(query).subscribe({
  //     next: results => this.store.dispatch(musicSearchSuccess({ data: results })),
  //     error: error => this.store.dispatch(musicSearchFailure({ error })),
  //   })

}
