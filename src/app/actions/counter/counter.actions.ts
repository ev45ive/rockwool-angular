import { createAction, props } from '@ngrx/store';

export const loadCounters = createAction(
  '[Counter] Load Counters'
);

export const incrementCounter = createAction(
  '[Counter] Increment Counter',props<{
    amount: number
  }>()
);

export const decrementCounter = createAction(
  '[Counter] Increment Counter',props<{
    amount: number
  }>()
);




