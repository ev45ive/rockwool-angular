import * as fromMusicSearch from './music-search.actions';

describe('loadMusicSearchs', () => {
  it('should return an action', () => {
    expect(fromMusicSearch.musicSearchStart().type).toBe('[MusicSearch] Load MusicSearchs');
  });
});
