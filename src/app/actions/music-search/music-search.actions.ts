import { createAction, props } from '@ngrx/store';
import { Album } from 'src/app/core/models/album';

export const musicSearchStart = createAction(
  '[MusicSearch] Search Start', props<{
    query: string
  }>()
);

export const musicSearchSuccess = createAction(
  '[MusicSearch] Search Success',
  props<{ data: Album[] }>()
);

export const musicSearchFailure = createAction(
  '[MusicSearch] Search Failure',
  props<{ error: any }>()
);
