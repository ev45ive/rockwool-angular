import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { NgrxSearchComponent } from './containers/ngrx-search/ngrx-search.component';

import { MusicSearchComponent } from './music-search.component';

const routes: Routes = [
  {
    path: /* /music/.. */ '',
    redirectTo: 'ngrxsearch',
    pathMatch: 'full'
  },
  {
    path: /* /music/.. */ 'ngrxsearch',
    component: NgrxSearchComponent
  },
  {
    path: /* /music/.. */ 'search',
    component: MusicSearchComponent
  },
  //  [routerLink]="['/music/albums/',result.id]"
  { path: 'albums/:album_id', component: AlbumDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
