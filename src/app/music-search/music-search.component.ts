import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, pluck } from 'rxjs/operators';
import { Album, Artist, Track } from '../core/models/album';
import { MusicSearchService } from '../core/services/music-search/music-search.service';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {

  results = this.service.resultsChanges
  message = this.service.errorNotifications
  query = this.service.queryChanges

  albums: Album[] = []
  arists: Artist[] = []
  tracks: Track[] = []
  currentType = 'album'

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService) { }

  ngOnInit(): void {
    this.route.queryParamMap.pipe(map(pm => pm.get('q')))
      .subscribe(q => {
        if (!q) { return }
        this.service.requestAlbums(q)
      })
  }

  // searchAlbums(query: { form value ....}) {

  searchAlbums(query: string) {
    this.router.navigate(['/music', 'search'], {
      queryParams: { q: query }
    })
  }

}

// this.results = this.service.fetchSearchAlbumsResult(query)
//   .pipe(
//     tap(results => this.albums = results),
//     shareReplay()
// )
