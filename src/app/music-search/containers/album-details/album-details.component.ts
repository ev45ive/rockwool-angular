import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { Album, Track } from 'src/app/core/models/album';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';
import { PlayerService } from 'src/app/core/services/player/player.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {
  message = ''

  album_id$ = this.route.paramMap.pipe(map(pm => pm.get('album_id')))

  album$ = this.album_id$.pipe(
    switchMap(id => this.service.fetchAlbumById(id!)),
    catchError(err => { this.message = err.message; return EMPTY })
  )

  selectedTrack?: Track

    constructor(
      private player: PlayerService,
      private route: ActivatedRoute,
      private service: MusicSearchService) { }

  selectTrack(track: Track) {
    this.selectedTrack = track
  }

  addToPlaylist(track: Track) { 
    this.player.enqueue(track)
  }

  playTrack(track: Track) {
    this.player.play(track)
   
  }

  ngOnInit(): void {

  }

}
