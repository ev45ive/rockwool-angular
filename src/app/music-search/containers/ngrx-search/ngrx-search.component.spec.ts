import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxSearchComponent } from './ngrx-search.component';

describe('NgrxSearchComponent', () => {
  let component: NgrxSearchComponent;
  let fixture: ComponentFixture<NgrxSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgrxSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
