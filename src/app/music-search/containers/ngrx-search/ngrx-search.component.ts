import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { musicSearchFailure, musicSearchStart, musicSearchSuccess } from 'src/app/actions/music-search/music-search.actions';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';
import { AppState } from 'src/app/reducers';
import { selectErrorMessage, selectLoading, selectSearchQuery, selectSearchResults } from 'src/app/selectors/music-search/music-search.selectors';

@Component({
  selector: 'app-ngrx-search',
  templateUrl: './ngrx-search.component.html',
  styleUrls: ['./ngrx-search.component.scss']
})
export class NgrxSearchComponent implements OnInit {

  results = this.store.select(selectSearchResults)
  loading = this.store.select(selectLoading)
  message = this.store.select(selectErrorMessage)
  query = this.store.select(selectSearchQuery)

  constructor(private store: Store<AppState>/* ,
    private service: MusicSearchService */) { }

  search(query: string) {
    this.store.dispatch(musicSearchStart({ query }))
   /*  this.service.fetchSearchAlbumsResult(query).subscribe({
      next: results => this.store.dispatch(musicSearchSuccess({ data: results })),
      error: error => this.store.dispatch(musicSearchFailure({ error })),
    }) */
  }

  ngOnInit(): void {
  }

}
