import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { StoreModule } from '@ngrx/store';
import * as fromMusicSearch from '../reducers/music-search/music-search.reducer';
import { EffectsModule } from '@ngrx/effects';
import { MusicSearchEffects } from '../effects/music-search/music-search.effects';
import { NgrxSearchComponent } from './containers/ngrx-search/ngrx-search.component';


@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    AlbumDetailsComponent,
    NgrxSearchComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule,
    StoreModule.forFeature(fromMusicSearch.musicSearchFeatureKey, fromMusicSearch.reducer),
    EffectsModule.forFeature([MusicSearchEffects])
  ]
})
export class MusicSearchModule { }
