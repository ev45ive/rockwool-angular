import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { combineLatest, Observable, Observer, of, pipe, Subscriber } from 'rxjs';
import { debounceTime, distinctUntilChanged, distinctUntilKeyChanged, filter, map, mapTo, withLatestFrom } from 'rxjs/operators';
import { AbstractControl, AsyncValidatorFn, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

// declare global{
//   interface Window{
//     form: FormGroup
//   }
// }

const censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const badword = 'batman'
  const hasErr = String(control.value).includes(badword)

  return hasErr ? { 'censor': { badword } } : null
  // return { required: true, minlength: { requiredLength: 123 } }
}

// https://rxjs-dev.firebaseapp.com/api/index/function/bindCallback
// https://rapidapi.com/neutrinoapi/api/bad-word-filter
const asyncCensor: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
  // return this.http.get('/validat...?v='+control.value).pipe(map(res=> error | null ))

  return new Observable((subscriber: Observer<ValidationErrors | null>) => {
    // console.log('on subscribe')

    const resp = censor(control)
    const handler = setTimeout(() => {
      // console.log('on validation (next)')
      // subscriber.error(resp) // pending...
      subscriber.next(resp)
      subscriber.complete()
    }, 100)

    return () => {
      // console.log('on unsubscribe')
      clearTimeout(handler)
    }
  })
  // .subscribe(nextFn)
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Output() search = new EventEmitter<string>();

  @Input() set query(query: string | null) {
    query && this.queryField.setValue(query, {
      emitEvent: false,
      onlySelf: true,
    })
  }

  // ngOnChanges(changes: SimpleChanges): void {
  //   //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
  //   //Add '${implements OnChanges}' to the class.

  // }

  searchForm = new FormGroup({
    // query: this.queryField,
    'query': new FormControl('', {
      validators: [
        // censor,
        Validators.required,
        Validators.minLength(3)
      ],
      asyncValidators: [asyncCensor]
    }),
    'extras': new FormGroup({
      'type': new FormControl('album', {}),
      'markets': new FormArray([])
    })
  })
  queryField = this.searchForm.get('query') as FormControl
  markets = this.searchForm.get('extras.markets') as FormArray
  extras = false

  constructor() {
    (window as any).form = this.searchForm
    this.addMarket('PL')

  }

  ngOnInit(): void {
    const statusChanges = this.queryField!.statusChanges as Observable<'VALID' | 'INVALID' | 'PENDING' | 'DISABLED'>
    const valueChanges = this.queryField!.valueChanges as Observable<string>

    const validStatuses = statusChanges.pipe(
      debounceTime(400),
      filter(status => status === 'VALID'),
      mapTo(true),
    );

    const switchToValuesFrom = function <T>(values: Observable<T>) {
      return pipe(
        withLatestFrom(values),
        map(([status, value], count) => value),
        o => o,
        distinctUntilChanged(),
      )
    }
    // node_modules\rxjs\internal\operators\map.js  => MapOperator

    const searchChanges = validStatuses.pipe(switchToValuesFrom(valueChanges));

    searchChanges.subscribe(this.search)

    // .subscribe(console.log)


    // statusChanges.pipe(
    //  withLatestFrom(valueChanges),
    //  filter(([status, value]) => status === 'VALID'),
    //    map(([status, value]) => value),
    //    o => o
    // ).subscribe(console.log)


    // combineLatest([statusChanges, valueChanges]).pipe(
    //   filter(([status, value]) => status === 'VALID'),
    //   map(([status, value]) => value),
    //   o => o
    // ).subscribe(console.log)


    // valueChanges.pipe(
    //   // wait for user to stop typing then search,
    //   debounceTime(400),
    //   // minimum 3 characters
    //   filter(query => query.length >= 3),
    //   // no duplicates
    //   distinctUntilChanged(),
    // )
    //   .subscribe(this.search) // propagates all next`s, error and complete to subject

  }

  addMarket(code = '') {
    this.markets.push(new FormGroup({
      'code': new FormControl(code, {}),
    }))
  }

  removeMarket(index: number) {
    this.markets.removeAt(index)
  }


  submitSearch() {
    this.search.emit(this.queryField.value)
  }

}
