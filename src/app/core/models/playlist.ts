export interface Entity {
  id: string;
  name: string;
}

export interface Playlist extends Entity {
  type: 'playlist'
  public: boolean;
  description: string;
  /**
   * List of Tracks
   * @author placki!
   */
  tracks?: Track[];
}

export interface Track extends Entity {
  type: 'track' // type discriminator
  duration: number
}

// // Tagged union
// const result: Playlist | Track = {} as Playlist | Track;

// if(result.type == 'playlist'){
//   result.tracks
// }else{
//   result.duration
// }

// switch(result.type){
//   case 'playlist': result.tracks; break;
//   case 'track': result.duration; break;
// }

// export interface Track extends Entity {
//   type: 'track'
// }

// const track: Track = {
//   id: '123',
//   name: '123',
//   duration: 123,
//   type: 'track'
// }

// const p: Playlist = {} as Playlist

// if (p.tracks) { p.tracks.length }
// p.tracks ? p.tracks.length : 0
// p.tracks && p.tracks.length
// p.tracks?.length

// // const  x = p.tracks?.length
// const x = p.tracks!.length

// export interface Entity {
//   id: string | number;
// }
// const p:Entity = { id:123 }
// // p.id = '123'
// p.id.toString()

// if(typeof p.id === 'string'){
//   p.id.toUpperCase()
// }else{
//   p.id.toExponential()
// }

// interface Vector { x: number, y: number, length: number }
// interface Point { x: number, y: number }

// let v: Vector = { x: 12, y: 345, length: 12 }
// let p: Point = { x: 12, y: 345 }

// // v = p // Property 'length' is missing in type 'Point' but required in type 'Vector'.
// // p = v
