import { InjectionToken } from '@angular/core';
import { Playlist } from './models/playlist';

export const PLAYLISTS_MOCK = new InjectionToken<Playlist[]>('Playlists initial data');
export const ALBUMS_MOCK = new InjectionToken<Playlist[]>('Albums initial data');
export const API_URL = new InjectionToken<Playlist[]>('URL for music search API');



// DependencyInjection "by interface" ;-)

export const IPlaylistsAPIConnectable = //
  new InjectionToken('IPlaylistsAPIConnectable')

export interface IPlaylistsAPIConnectable { }
