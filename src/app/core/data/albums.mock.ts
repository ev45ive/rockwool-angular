import { Album } from '../models/album';

export const albumsMock: Pick<Album, 'id' | 'name' | 'images'>[] = [
  { id: '123', name: 'Test 123', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }] },
  { id: '234', name: 'Test 234', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }] },
  { id: '345', name: 'Test 345', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }] },
  { id: '466', name: 'Test 466', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/600/600' }] },
]

// type PartialAlbum = Album

// type PartialAlbum = {
//   id: Album['id']
//   name: Album['name']
//   images: Album['images']
// }

// type AlbumKeys = 'id' | 'name' | 'images'
// type PartialAlbum = {
//   [k in AlbumKeys]?: Album[k]
// }
// type ReadonlyAlbum = {
//   readonly [k in AlbumKeys]: Album[k]
// }

// type PartialAlbum = {
//   [k in keyof Album]?: Album[k]
// }

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }

// type Pick<T, K extends keyof T> = {
//   [k in K]: T[k]
// }
