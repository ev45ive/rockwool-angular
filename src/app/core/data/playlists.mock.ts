import { Playlist } from '../models/playlist';

export const playlistsMock: Playlist[] = [
  {
    id: '123',
    type: 'playlist',
    name: 'pLaylist 123',
    public: false,
    description: 'some descriptinsome de123escriptin',
    tracks: []
  },
  {
    id: '234',
    type: 'playlist',
    name: 'pLaylist 234',
    public: true,
    description: 'some descr123descriptin',
    tracks: []
  },
  {
    id: '345',
    type: 'playlist',
    name: 'pLaylist 345',
    public: false,
    description: 'some 12in',
    tracks: []
  },
]
