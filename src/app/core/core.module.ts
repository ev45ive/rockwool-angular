import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { API_URL } from './tokens';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './services/auth/auth.service';
import { OAuthModule, AUTH_CONFIG } from 'angular-oauth2-oidc';
import { AuthInterceptor } from './interceptors/auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    // { provide: HttpClient, useClass: MySuperExtraAuthHttpClient }
    // { provide: OAuthStorage, useFactory: () => window.localStorage }
    // { provide: AUTH_CONFIG, useValue: environment.authImplicitFlowConfig },
    {
      provide: API_URL,
      useValue: environment.api_url
    }
    // {
    //   provide: PLAYLISTS_MOCK,
    //   useValue: []
    // },
    // {
    //   provide: PlaylistsService,
    //   useFactory(data: Playlist[]/* configB, instanceC */) {
    //     return new PlaylistsService(data)
    //   },
    //   deps: [PLAYLISTS_MOCK/* ,tokenB, tokenC */]
    // },

    // {
    //   provide: AbstractPlaylistsService,
    //   useClass: SpotifyPlaylistsService,
    //   // deps: [PLAYLISTS_MOCK/* ,tokenB, tokenC */]
    // },

    // PlaylistsService,

    // Aliasing:
    // {provide: UserPlaylistsService, useExisting:PlaylistsService}
    // {provide: TopPlaylistsService, useExisting:PlaylistsService}
    // {provide: RecentPlaylistsService, useExisting:PlaylistsService}
  ]
})
export class CoreModule {

  constructor(
    private auth: AuthService,
    @Optional() @SkipSelf() coreModule: CoreModule) {
    if (coreModule !== null) {
      throw new Error('Core should be included only once in root module!')
    }

    this.auth.init()

    // console.log('core')
  }

  // constructor(@Inject(ROUTES) routes: Route[][]){
  //   console.log(routes)
  // }
}
