import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { Playlist } from '../models/playlist';
import { PlaylistsService } from '../services/playlists/playlists.service';

@Injectable({
  providedIn: 'root'
})
export class PlaylistResolver implements Resolve<Playlist | null> {


  constructor(private playlistService: PlaylistsService) { }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Playlist> {
    const id = route.paramMap.get('playlist_id')
    console.log(id)
    return this.playlistService.fetchPlaylistById(id!)
  }
}
