import { ErrorHandler, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';
import { catchError, concatMap, exhaustMap, map, mergeAll, mergeMap, switchMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private errorHandler: ErrorHandler,
    private auth: AuthService) { }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler): Observable<HttpEvent<unknown>> {

    const authRequest = this.authorizeRequest(request)

    return next.handle(authRequest).pipe(
      // retry(3)
      // retryWhen( errors => ... )
      catchError((error, caughtSourceRetry) => {
        if (!(error instanceof HttpErrorResponse)) {
          this.errorHandler.handleError(error)
          return throwError(new Error('Unexpected API error'))
        }

        if (error.status === 401) {
          // Show Popup and wait for close and new token
          return from(this.auth.login()).pipe(
            // HIgher Order  Observable ==>  Observable<Observable<T>>
            // mergeMap(() => next.handle(this.authorizeRequest(request))),
            concatMap(() => next.handle(this.authorizeRequest(request))),
            // switchMap(() => next.handle(this.authorizeRequest(request))), // debounce
            // exhaustMap(() => next.handle(this.authorizeRequest(request))),  // throttle
            // mergeAll() //  Observable<Observable<HttpEvent<any>>> =>  Observable<HttpEvent<any>>
          )
        }

        return throwError(new Error((error.error.error.message)))

      })
    )
  }

  private authorizeRequest(request: HttpRequest<unknown>) {
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    });
  }
}


// obs = HttpClient.request => A.handle

// A.next = B
// B.next = C
// C.next = HttpHandler.send

// obs.subscribe()
