import { TestBed } from '@angular/core/testing';

import { PlaylistPrivateGuard } from './playlist-private.guard';

describe('PlaylistPrivateGuard', () => {
  let guard: PlaylistPrivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PlaylistPrivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
