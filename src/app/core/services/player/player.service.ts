import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { Track } from '../../models/album';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private tracks: Track[] = []

  private track = new ReplaySubject<Track | null>(10)
  currentTrack = this.track.asObservable()

  play(track: Track) {
    this.track.next(track)
  }

  enqueue(track: Track) {
    this.tracks.push(track)
  }

  trackCompleted() {
    const track = this.tracks.shift()
    if (track)
      this.play(track)
  }

  constructor() { }
}
