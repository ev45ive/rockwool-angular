import { ErrorHandler, EventEmitter, Inject, Injectable, Optional } from '@angular/core';
import { Album, AlbumsResponse, AlbumsSearchResponse } from '../../models/album';
import { ALBUMS_MOCK, API_URL } from '../../tokens';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { asyncScheduler, AsyncSubject, BehaviorSubject, concat, EMPTY, from, merge, Observable, of, ReplaySubject, Subject, Subscription, throwError } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { catchError, concatAll, map, mergeAll, observeOn, pluck, tap } from 'rxjs/operators'
import { AsyncScheduler } from 'rxjs/internal/scheduler/AsyncScheduler';

@Injectable({
  providedIn: 'root'
})
export class MusicSearchService {
  private errors = new ReplaySubject<string>(10, 10_000)
  errorNotifications = this.errors.asObservable()

  private queries = new ReplaySubject<string>(10);
  queryChanges = this.queries.asObservable()

  private results = new BehaviorSubject<Album[]>([])
  resultsChanges = this.results.asObservable()

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private api_url: string,
    @Optional() @Inject(ALBUMS_MOCK) private resultsInit: Album[] | null
  ) {
    this.results.next(resultsInit || [])
      ; (window as any).subject = this.results
  }

  requestAlbums(query: string) {
    this.queries.next(query)

    this.fetchSearchAlbumsResult(query).subscribe({
      next: results => this.results.next(results),
      error: error => this.errors.next(error.message)
    })
  }

  fetchSearchAlbumsResult(query: string) {
    return this.http.get<AlbumsSearchResponse>(`${this.api_url}/search`, {
      params: {
        type: 'album', q: query
      },
    }).pipe(
      map(res => res.albums.items),
    )
  }

  fetchAlbumById(id: string) {
    return this.http.get<Album>(`${this.api_url}/albums/${id}`)
  }
}

