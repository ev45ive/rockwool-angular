import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private oAuth: OAuthService) {
    this.oAuth.configure(environment.authImplicitFlowConfig)
  }

  async init() {
    // Extract tokens from URL / Storage
    await this.oAuth.tryLogin({})
    const token = this.oAuth.getAccessToken()
    console.log(token)
    // if (!token) {
    //   this.login()
    // }
  }

  login() {
    // Initialize REdirect to auth page
    // this.oAuth.initLoginFlow()
    return this.oAuth.initLoginFlowInPopup({
      height: 600, width: 400
    })
  }

  logout() { }

  getToken() {
    return this.oAuth.getAccessToken()
  }
}
