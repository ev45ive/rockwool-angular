import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { API_URL, IPlaylistsAPIConnectable, PLAYLISTS_MOCK } from 'src/app/core/tokens';
import { PagingObject } from '../../models/album';
import { Playlist } from '../../models/playlist';

@Injectable({
  // providedIn: CoreModule
  // providedIn: 'any',
  providedIn: 'root',
})
export class PlaylistsService {

  private playlists = new BehaviorSubject<Playlist[]>([])
  playlistsChange = this.playlists.asObservable()

  constructor(
    @Inject(API_URL) private api_url: string,
    private http: HttpClient,
    @Optional() @Inject(PLAYLISTS_MOCK) private playlistsData: Playlist[]
  ) {
    this.playlists.next(playlistsData || [])
  }

  fetchCurrentUserPlaylists() {
    return this.http.get<PagingObject<Playlist>>(`${this.api_url}/me/playlists`).pipe(
      map(res => res.items)
    )
  }

  fetchPlaylistById(playlist_id: Playlist['id']) {
    return this.http.get<Playlist>(`${this.api_url}/playlists/${playlist_id}`)
  }

  loadPlaylists() {
    this.fetchCurrentUserPlaylists().subscribe({
      next: res => this.playlists.next(res)
    })
  }

  savePlaylist(draft: Playlist) {
    return this.http.put<Playlist>(`${this.api_url}/playlists/${draft.id}`, {
      name: draft.name,
      public: draft.public,
      description: draft.description
    }).pipe(
      tap(() => {
        this.loadPlaylists()
      })
    )
  }
}
