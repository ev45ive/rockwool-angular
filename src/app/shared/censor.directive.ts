import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[appCensor]',
  providers:[
    {
      provide: NG_VALIDATORS,
      useExisting: CensorDirective,
      // Mixing multi and non multi provider is not possible for token NG_VALIDATORS:
      multi:true
    }
  ]
})
export class CensorDirective implements Validator {

  constructor() { }
  validate(control: AbstractControl): ValidationErrors | null {
    return String(control.value).match(/placki/i) ? {
      censor: true
    } : null
  }

}
