import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appStopEvent]',
  // host:{'(click)':'func($event)'}
})
export class StopEventDirective {

  constructor() { }

  @HostListener('click', ['$event'])
  handleClick($event: MouseEvent) {
    $event.stopPropagation()
  }

}
