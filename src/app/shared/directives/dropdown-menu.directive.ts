import { Directive, ElementRef, HostBinding, HostListener, Inject, Optional } from '@angular/core';
import { DOCUMENT } from '@angular/common'
import { DropdownDirective } from './dropdown.directive';
@Directive({
  selector: '[appDropdownMenu]',
  exportAs: 'appDropdownMenu'
})
export class DropdownMenuDirective {

  @HostBinding('class.show')
  @HostBinding('attr.aria-expanded')
  open = false

  constructor(
    @Optional() private parent: DropdownDirective,
    // @Inject(DOCUMENT) private document: Document,
    private elem: ElementRef<HTMLElement>
  ) {
    if (this.parent) {
      this.parent.menu = this
    }
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }


  @HostListener('document:click', ['$event.path'])
  documentClick(path: HTMLElement[]) {
    // if (this.open && !path.includes(this.elem.nativeElement)) {
    //   this.open = false
    // }
  }


  toggle() {
    // this.elem.nativeElement.classList.toggle('show')
    this.open = !this.open;
  }

}
