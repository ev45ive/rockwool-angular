import { Directive } from '@angular/core';
import { DropdownMenuDirective } from './dropdown-menu.directive';
import { DropdownToggleDirective } from './dropdown-toggle.directive';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  toggle?: DropdownToggleDirective;
  menu?: DropdownMenuDirective;

  constructor() { }

}
