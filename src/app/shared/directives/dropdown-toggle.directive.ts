import { Directive, ElementRef, HostListener, Input, Optional, Renderer2 } from '@angular/core';
import { DropdownMenuDirective } from './dropdown-menu.directive';
import { DropdownDirective } from './dropdown.directive';

@Directive({
  selector: '[appDropdownToggle]',
  host: {
    // '(click)': 'toggleDropdown($event.button)',
  }
})
export class DropdownToggleDirective {

  @Input() appDropdownToggle?: DropdownMenuDirective | ''

  constructor(
    @Optional() private parent: DropdownDirective,
    private renderer: Renderer2,
    private elem: ElementRef<HTMLElement>
  ) {
    // renderer.listen(elem.nativeElement,'click',console.log)

    if (this.parent) {
      this.parent.toggle = this
    }
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.appDropdownToggle = this.parent.menu
  }

  @HostListener('click', ['$event'])
  toggleDropdown(event: MouseEvent) {
    event.stopPropagation();
    (this.appDropdownToggle as DropdownMenuDirective)?.toggle()
  }

}
