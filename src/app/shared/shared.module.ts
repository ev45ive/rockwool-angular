import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './yesno.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CensorDirective } from './censor.directive';
import { CardComponent } from './card/card.component';
import { DropdownMenuDirective } from './directives/dropdown-menu.directive';
import { DropdownToggleDirective } from './directives/dropdown-toggle.directive';
import { DropdownDirective } from './directives/dropdown.directive';
import { RecentSearchesComponent } from './components/recent-searches/recent-searches.component';
import { LoadingComponent } from './loading/loading.component';
import { TracksListComponent } from './components/tracks-list/tracks-list.component';
import { DurationPipe } from './pipes/duration.pipe';
import { StopEventDirective } from './directives/stop-event/stop-event.directive';
import { PlayerComponent } from './components/player/player.component';
import { RouterMessageComponent } from './components/router-message/router-message.component';


@NgModule({
  declarations: [
    YesnoPipe,
    CensorDirective,
    CardComponent, DropdownDirective,
    DropdownToggleDirective,
    DropdownMenuDirective,
    RecentSearchesComponent,
    LoadingComponent,
    TracksListComponent,
    DurationPipe,
    StopEventDirective,
    PlayerComponent,
    RouterMessageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    YesnoPipe,
    FormsModule,
    ReactiveFormsModule,
    CensorDirective,
    CardComponent, DropdownDirective,
    DropdownToggleDirective,
    DropdownMenuDirective,
    RecentSearchesComponent,
    LoadingComponent,
    TracksListComponent,
    PlayerComponent,
    RouterMessageComponent
  ]
})
export class SharedModule { }
