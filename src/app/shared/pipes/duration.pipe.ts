import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration',
  // pure: false
})
export class DurationPipe implements PipeTransform {

  transform(value: number): unknown {
    const d = new Date(value)
    return `${('0' + d.getMinutes()).slice(-2)}:${('0' + d.getSeconds()).slice(-2)}`;
  }

}
