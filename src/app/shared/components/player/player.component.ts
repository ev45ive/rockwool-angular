import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Track } from 'src/app/core/models/album';
import { PlayerService } from 'src/app/core/services/player/player.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  currentTrack?: Track | null;

  constructor(
    private service: PlayerService
  ) { }

  ngOnInit(): void {
    this.service.currentTrack.subscribe(track => {
      this.currentTrack = track
      if (track == undefined) { return }
      this.play(track)
    })
  }

  ended(event:any){
    this.service.trackCompleted()
  }

  @ViewChild('ref')
  audioRef?: ElementRef<HTMLAudioElement>

  play(track: Track) {
    if (this.audioRef) {
      this.audioRef.nativeElement.src = track.preview_url
      this.audioRef.nativeElement.volume = 0.2
      this.audioRef.nativeElement.play()
    }

  }

}
