import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterMessageComponent } from './router-message.component';

describe('RouterMessageComponent', () => {
  let component: RouterMessageComponent;
  let fixture: ComponentFixture<RouterMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouterMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
