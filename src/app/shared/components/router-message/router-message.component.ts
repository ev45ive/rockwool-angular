import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { pluck } from 'rxjs/operators';

@Component({
  selector: 'app-router-message',
  templateUrl: './router-message.component.html',
  styleUrls: ['./router-message.component.scss']
})
export class RouterMessageComponent implements OnInit {

  message = this.route.data.pipe(pluck('message'))
  
  constructor(private route: ActivatedRoute) { }
  
  ngOnInit(): void {
  }

}
