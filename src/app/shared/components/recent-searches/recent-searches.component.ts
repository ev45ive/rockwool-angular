import { Component, Input, OnInit } from '@angular/core';
import { scan } from 'rxjs/operators';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-recent-searches',
  templateUrl: './recent-searches.component.html',
  styleUrls: ['./recent-searches.component.scss']
})
export class RecentSearchesComponent implements OnInit {

  @Input() count = 5;

  recentQueries: string[] = ['alice', 'bob', 'test']

  constructor(private service: MusicSearchService) { }

  search(query: string) {
    this.service.requestAlbums(query)
  }

  ngOnInit(): void {

    const recentQueries = this.service.queryChanges.pipe(
      scan((recentList, query) => {
        recentList.unshift(query)
        return recentList.slice(0, this.count)
      }, this.recentQueries)
    )

    recentQueries.subscribe(//
      recentList => this.recentQueries = recentList
    )


    // this.service.queryChanges.subscribe((query: string) => {
    //   this.recentQueries.unshift(query)
    //   this.recentQueries = this.recentQueries.slice(0, 5)
    // })
  }

}
