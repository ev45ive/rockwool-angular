import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Track } from 'src/app/core/models/album';

@Component({
  selector: 'app-tracks-list',
  templateUrl: './tracks-list.component.html',
  styleUrls: ['./tracks-list.component.scss']
})
export class TracksListComponent implements OnInit {

  @Input() tracks?: Track[] = [];
  @Input() selected?: Track['id'] 

  @Output() select = new EventEmitter<Track>();
  @Output() play = new EventEmitter<Track>();
  @Output() add = new EventEmitter<Track>();

  constructor() { }

  selectTrack(track: Track) {
    this.select.emit(track)
  }

  playTrack(track: Track) {
    this.play.emit(track)
  }

  addToPlaylist(track: Track) {
    this.add.emit(track)
  }

  ngOnInit(): void {
  }

}
