# GIT
git clone https://bitbucket.org/ev45ive/rockwool-angular.git rockwool-angular
cd rockwool-angular
npm i 
npm run start
<!-- OR -->
npm run start -- -o
<!-- npm run ng -- g .... -->
git stash -u -m "Co ja tu robie.."
git pull 

# Instalacje
chrome
node -v 
npm -v 
git --version

# Mac NPM no sudo
https://medium.com/@ExplosionPills/dont-use-sudo-with-npm-still-66e609f5f92

# Angular CLI
npm install -g @angular/cli
npm install -g @angular/cli@11.0.2
ng --version 

# Generate new app with CLI
cd ..
ng new angular-rockwool
? Do you want to enforce stricter type checking and stricter bundle budgets in the workspace?
  This setting helps improve maintainability and catch bugs ahead of time.
  For more information, see https://angular.io/strict Yes
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss            
]
# Serve 
ng serve -o 
ng s 

npm run start
<!-- npm run ng -- serve -o -->
# Update
https://update.angular.io/?l=3&v=10.0-11.0

# Linter
tslint.json
ng lint
npm run ng -- lint
prettier 
https://prettier.io/docs/en/precommit.html
https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

# Vs Code extensions
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
https://marketplace.visualstudio.com/items?itemName=cyrilletuzi.angular-schematics 
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher
https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig

# Chrome Ext - Augury
https://chrome.google.com/webstore/detail/augury/elgalmkoelokbchhkhacckoklkejnhcd


# Structure
## Data/Busienss Layer
## Core shared Domain and business logic (import only once, only in app.module)
ng g m core -m app
ng g i core/models/Playlist
ng g s core/services/playlists-api
ng g s core/services/playlists

## Application/Navigation Layer
## Feature modules
// Root Feature
ng g m playlists -m app --routing 
ng g c playlists/containers/playlists-view --type container
ng g c playlists/components/playlist-details-widget

// Lazy Loaded Feature
ng g m search -m app --route search 

## UI/Presentation Layer
## Common/shared Declaration (component,pipe, directive), imported in multiple feature modules
ng g m shared 
ng g p yesno --export

# Schematics - Playlists
<!-- git commit ... -->
npm run ng -- g m --name=playlists --module=app --routing 

npm run ng -- g c --name=playlists/containers/playlists --module=playlists --type=container 

npm run ng -- g c --name=playlists/components/playlists-list
npm run ng -- g c --name=playlists/components/playlists-list-item
npm run ng -- g c --name=playlists/components/playlist-details
npm run ng -- g c --name=playlists/components/playlist-edit-form
<!-- git reset --hard HEAD -->

# LIFT
https://angular.io/guide/styleguide#lift


# Emmet
https://docs.emmet.io/cheat-sheet/
table>thead>tr>th{lp}+th{name}^^tbody>tr*3>td{$}+td{Name $}

# Shared module
ng g m shared -m playlists
ng g p shared/yesno --export


# Music Search Feature

ng g m music-search --route music -m app
ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

ng g s core/services/music-search

# HomeWork - Playlist Tracks Editor Screen
<!-- route /playlists/tracks -->
ng g s core/playlists/tracks --flat false
ng g c playlists/containers/playlist-tracks
<!-- Mock ONE fake playlist with 2-3 tracks -->
ng g c playlists/component/tracks-list
<!-- select track -->
ng g c playlists/component/track-form
<!-- edit track name + save() -->

# Directives
ng g m playground --route playground -m app

ng g c playground/components/tabs
ng g c playground/components/tab

ng g d playground/directives/dropdown
ng g d playground/directives/dropdown-toggle
ng g d playground/directives/dropdown-menu

# Oauth2
https://auth0.com/docs/quickstart/spa/angular#install-the-auth0-angular-sdk
https://manfredsteyer.github.io/angular-oauth2-oidc/docs/

npm i angular-oauth2-oidc --save

# Album details 
(src/app/music-search/containers/album-details/album-details.component.ts)
ng g c music-search/containers/album-details

routing: (src\app\music-search\music-search-routing.module.ts)
/music/albums?id=5Tby0U5VndHW0SomYO7Id7

fetch data:
loadedAlbum = service.fetchAlbumById(id)

render data:
<app-album-card [album]="loadedAlbum">

Link to album details
routerLink="['/music','albums',{q:id}]"

# Homework
Album details
- add list of artists

Album details Links to artist and track detail pages:
<!-- /music/artist/5Tby0U5VndHW0SomYO7Id7 -->
ng g c music-search/containers/artist-details
<!-- /music/track/5Tby0U5VndHW0SomYO7Id7: -->
ng g c music-search/containers/track-details


# Homework EXTRA HARD
Search different types: Album | Artist | Track
/music/search/
  albums: Album[] = []
  arists: Artist[] = []
  tracks: Track[] = []
  currentType = 'album'

# Proxy requests
To set it up, we need to create a file proxy.conf.json at the root of our Angular CLI project. The content should look as follows

{
  "/api/*": {
    "target": "http://localhost:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  }
}
All requests made to /api/... from within our application will be forwarded to http://localhost:3000/api/....
https://juristr.com/blog/2016/11/configure-proxy-api-angular-cli/#configuring-your-angular-cli-dev-server-proxy


# Global Player

ng g s core/services/player --flat false
ng g c shared/components/player --export


# Testing
https://github.com/ngneat/spectator
https://material.angular.io/cdk/test-harnesses/overview


# NgRX
npm install --save @ngrx/store @ngrx/effects @ngrx/entity @ngrx/store-devtools @ngrx/schematics

## Store
ng generate @ngrx/schematics:store --name=app --module=app --no-flat --root --stateInterface=AppState --no-interactive

## Actions
ng generate @ngrx/schematics:action --name=counter --no-flat --group --no-interactive

## Reducer
ng generate @ngrx/schematics:reducer --name=counter --module=app --no-flat --group --reducers=reducers/index.ts --no-interactive 

## Container component
ng generate @ngrx/schematics:container --name=playlists/counter --state=../reducers/index.ts --stateInterface=AppState --no-interactive

## Feature reducer
 ng generate @ngrx/schematics:feature --name=music-search --module=music-search --api --no-flat --group --reducers=reducers/index.ts --no-interactive